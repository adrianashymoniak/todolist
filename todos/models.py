from __future__ import unicode_literals
from django.db import models
from datetime import datetime


class Todo(models.Model):
    title = models.CharField(max_length=200)
    text = models.TextField()
    created_at = models.DateTimeField(default=datetime.now, blank=True)
    edited_at = models.DateTimeField(blank=True)

    def __str__(self):
        return self.title


class Deadline(models.Model):
    value = models.DateTimeField(blank=True)
    todo_id = models.IntegerField()
