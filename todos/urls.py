from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^details/(?P<id>\w{0,50})/$', views.details),
    url(r'^add', views.add, name='add'),
    url(r'^change/(?P<id>\w{0,50})/$', views.change),
    url(r'^delete/(?P<id>\w{0,50})/$', views.delete),
    url(r'^delete_all', views.delete_all)
]
