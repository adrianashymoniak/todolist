from datetime import datetime

from django.shortcuts import render, redirect

from .models import Todo
from .models import Deadline


def index(request):
    todos = Todo.objects.all()
    deadlines = Deadline.objects.all()
    context = {
        'todos': todos,
        'deadlines': deadlines
    }

    return render(request, 'index.html', context)


def details(request, id):
    todo = Todo.objects.get(id=id)
    deadline_filtered = Deadline.objects.filter(todo_id=id)
    deadline = deadline_filtered[len(deadline_filtered) - 1]
    context = {
        'todo': todo,
        'deadline': deadline
    }

    return render(request, 'details.html', context)


def add(request):
    if request.method == 'POST':
        title = request.POST['title']
        text = request.POST['text']
        value = request.POST['value']

        todo = Todo(title=title, text=text)
        todo.save()

        todos_all = Todo.objects.all()
        id = todos_all[len(todos_all) - 1].id

        deadline = Deadline(value=value, todo_id=id)
        deadline.save()

        return redirect('/todos')
    else:
        return render(request, 'add.html')


def change(request, id):
    if request.method == 'POST':
        todo = Todo.objects.get(id=id)
        todo.title = request.POST['title']
        todo.text = request.POST['text']
        todo.edited_at = datetime.now()

        todo.save()

        todos_all = Todo.objects.all()
        new_id = todos_all[len(todos_all) - 1].id
        value = request.POST['value']
        deadline = Deadline(value=value, todo_id=new_id)

        deadline.save()

        return redirect('/todos')
    else:
        todo = Todo.objects.get(id=id)
        context = {
            'todo': todo
        }

        return render(request, 'change.html', context)


def delete(request, id):
    Deadline.objects.filter(todo_id=id).delete()
    todo = Todo.objects.get(id=id)
    todo.delete()

    return redirect('/todos')


def delete_all(request):
    Deadline.objects.all().delete()
    Todo.objects.all().delete()

    return redirect('/todos')
